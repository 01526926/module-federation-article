import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ArticleService} from '../service/article.service';
import {Article} from '../model/article';

@Component({
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  articles: Article[] = [];
  activeArticle: Article = {id: 0, name: '', price: '', active: false};
  form: any;

  constructor(private formBuilder: FormBuilder, private articleService: ArticleService) {
  }

  ngOnInit(): void {
    this.articleService.getArticles().subscribe(a => {
      this.articles = a;
      this.activeArticle = this.articles[0];
    });
    this.form = this.formBuilder.group({
      id: [0],
      name: [''],
      price: [''],
      active: [false]
    });
  }

  onRowClick(rowIndex: number) {
    this.setActive(rowIndex);
  }

  onSave() {
    alert('Save data was clicked.');
  }

  private setActive(rowIndex: number) {
    this.activeArticle = this.articles[rowIndex];
  }
}
