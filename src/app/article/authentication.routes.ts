import {Routes} from '@angular/router';
import {ArticleComponent} from './article/article.component';

export const ARTICLE_ROUTES: Routes = [
  {
    path: '',
    component: ArticleComponent
  }
]
