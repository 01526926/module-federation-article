import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Article} from '../model/article';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  articles = [
    {
      id: 123452,
      name: 'Toothbrush',
      price: '€ 126.00',
      active: true
    },
    {
      id: 821934,
      name: 'Chair',
      price: '€ 124.87',
      active: false
    }
  ];

  private static readonly ARTICLE_ENDPOINT = environment.apiUrl + '/article';

  constructor(/* private http: HttpClient */) {
  }

  getArticles(): Observable<Article[]> {
    return of(this.articles);
    // // This does not work currently due to HttpClient injection error
    // return this.http.get<Article[]>(ArticleService.ARTICLE_ENDPOINT); //of([]);
  }
}
